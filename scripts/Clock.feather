/**
    The Clock can appear in the Doll's House, or a Play scene.
    The hands show the actual time :-)
*/
class Clock : ActionRole(), Reward {

    @Attribute( about="Which scene must be completed to see this item in a Doll's House?" )
    var rewardForScene: String = ""
    override fun rewardForScene() = rewardForScene


    var minuteHand: Actor
    var hourHand: Actor

    override fun activated() {
        super.activated()

        minuteHand = actor.createChild("minute-hand")
        hourHand = actor.createChild("hour-hand")
        updateHands()
    }

    override fun createAction(): Action {
        return (Delay(30.0) thenOnce this:>updateHands ).forever()

    }

    fun updateHands() {
        val cal = Calendar.getInstance()
        val minutes = cal.get(Calendar.MINUTE)
        val hours = cal.get(Calendar.HOUR)

        minuteHand.direction.degrees = 90.0 - minutes * 6.0
        hourHand.direction.degrees = 90.0 - (hours + minutes / 60.0) * 30.0
    }

}
