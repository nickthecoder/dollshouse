/**
    Handles all of the "global" stuff, such as saving preferences, and laying out the views.

    It also highlights newly aquired Rewards when a scene starts.
    I chose to put this code here, because this is where [newlyCompletedScene] is stored.
    This value needs to survice from one scene to the next, and therefore can't live on a Director.
*/
class DollsHouseProducer : AbstractProducer {

    /**
        When we complete a Play scene, we remember the scene's name, so that we can highlight NEW rewards in the
        DollsHouse scene.
    */
    var newlyCompletedScene = ""

    val sceneTransition = SceneTransition()

    override fun begin() {
        loadWindowSize()
        Options.instance.load()
    }

    override fun end() {
        saveWindowSize()
    }


    override fun sceneEnding() {
        sceneTransition.prepareTransition() // Takes a screenshot of the old scene
    }

    override fun sceneLoaded() {
        val actor = sceneTransition.actor
        if (actor != null) {
            sceneTransition.beginTransition( Fade( actor, 0.5, 0 ) ) // Becomes transparent in 0.5 seconds.
        }
    }

    override fun sceneActivated() {
        // If we just completed a Play scene, and have started a DollsHouse scene, then highlight
        // new rewards (such as dolls and scenery)
        if (newlyCompletedScene != "") {
            var foundNew = false
            for ( reward in Game.instance.scene.findStage("main").findRolesByClass<Reward>( Reward ) ) {
                if (reward.rewardForScene() == newlyCompletedScene) {
                    DollsHouseDirector.instance.showNewReward( reward )
                    foundNew = true
                }
                if ( foundNew ) {
                    playSound( "pop" )
                }
            }
            newlyCompletedScene = ""
        }
    }

    override fun layout() {
        // Changes the size/scale of the views based on GameInfo.width, GameInfo.height,
        // and the details of each view in the "Layout"
        arrangeViews()
    }

    override fun message( message : String ) {
        if (message == "clearRewards") {
            DollsHouseProducer.clearRewards()
            startScene( "menu" )
        }
    }

    // static methods...
    
    static fun scenePreferences() = Game.instance.preferences.node("scenes")
    static fun scenePreferences(sceneName: String) = scenePreferences().node(sceneName)

    static fun isSceneCompleted( sceneName: String) = scenePreferences(sceneName).getBoolean("completed", false)

    static fun sceneCompleted( sceneName: String, time : double ) {
        val preferences = scenePreferences(Game.instance.sceneName)
        preferences.putBoolean("completed", true)
        preferences.putDouble("time", time)
        (producer as DollsHouseProducer).newlyCompletedScene = sceneName
    }

    /**
        All rewards will be removed from the Doll's Houses (as if you've not completed any levels).
    */
    static fun clearRewards() {
        scenePreferences().removeNode()
    }


    static fun timeString(seconds: int) = "${seconds ~/ 60}:${String.format("%02d", (seconds % 60))}"


}
