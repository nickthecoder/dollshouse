abstract class Collectable : ActionRole(), ContactListenerRole {

    @Attribute( about="The item moves in a circle" )
    var radius = 30.0

    @Attribute( about="The time it takes to travel round in a circle" )
    var period = 2.0


    var collected = false


    override fun createAction(): Action {
        return (
            Circular(actor.position, period/2, radius)
        )
        .forever()
    }

    override fun beginContact(contact: Contact, otherActor: Actor) {
        val otherRole = otherActor.role
        if (!collected && otherRole is DollPart && !(otherRole as DollPart).doll.ending) {
            collected = true
            collected()
            replaceAction(
                MoveBy( actor.position, 0.3, Vector2(0,-20), Eases.easeOut ) then (
                    MoveBy(actor.position, 1.0, Vector2(0, 200)) and
                    Fade(actor.color, 1.0, 0f, Eases.easeIn)
                ) then
                Kill(actor)
            )
        }
    }

    override fun endContact(contact: Contact, otherActor: Actor) {}

    abstract fun collected()
}

class CollectableGrow : Collectable() {

    @Attribute( about="How much do Dolls fired from the launcher grow by when this is collected" )
    var factor = 1.2

    override fun collected() {
        Play.instance.launcher.scale *= factor
    }
}

class CollectableShrink : Collectable() {

    @Attribute( about="How much do Dolls fired from the launcher shrink by when this is collected" )
    var factor = 1.2

    override fun collected() {
        Play.instance.launcher.scale /= factor
    }
}

class CollectableSpeedUp : Collectable() {

    @Attribute( about="How much faster are launched dolls once this is collected" )
    var factor = 1.2

    override fun collected() {
        Play.instance.launcher.speed *= factor
    }
}

class CollectableSlowDown : Collectable() {

    @Attribute( about="How much slower are launched dolls once this is collected" )
    var factor = 1.2

    override fun collected() {
        Play.instance.launcher.speed /= factor
    }
}
