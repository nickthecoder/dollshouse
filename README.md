# Doll's House

Play with articulated dolls in this virtual doll's house.
Complete challenges and be awarded with new dolls and other
items such as tables and chairs, vases, crockery...

![screenshot](screenshot.png)

See more [screenshots](screenshots/screenshots.md) as well game-play descriptions.

Doll's House was originally called
[Rapid Rag Doll.](http://nickthecoder.co.uk/software/rapidragdoll).
This version was ported from Kotlin to Tickle's preferred scripting language,
[Feather](http://nickthecoder.co.uk/software/feather).

The game-code is well below 2,000 lines of code, which I think is quite small,
(most of the hard work is handled by
[Tickle](http://nickthecoder.co.uk/software/tickle) and
[JBox2D](http://www.jbox2d.org/)
)

As with all Tickle games, Doll's House has a built-in scene editor.
So without writing any code, you could create your own custom Doll's House.
The editor is a little funky though; there's a learning curve!
If you are good with Photoshop or Gimp, you could turn photos of loved ones
into articulated dolls.

Progress
--------

The game is fully working, with a some rough edges, and probably a few bugs!

It could do with more sound effects (not all the dolls have voices for example).

The physics data needs tweaking too, e.g. setting the masses of each object,
their friction, coefficients of restitution  etc.

The Christmas themed levels aren't complete (3 missing).

Install
-------

To play the game, you must first compile
[Tickle](http://nickthecoder/software/tickle).

Start Tickle, and then click "Play", and choose the file : "dollsHouse.tickle".
(If you set up the appropriate file associations for ".tickle" files, you can double click it instead).

------------

Powered by [Tickle](http://nickthecoder.co.uk/software/tickle)
[LWJGL](https://www.lwjgl.org/) and
[JBox2D](http://www.jbox2d.org/)

