Screenshots
===========

[![Shah's Palace](shahMini.png)](shah.png)

[![Christmas Capers](xmasMini.png)](xmas.png)

Here we see two Doll's Houses.
Each doll's house can have many rooms, and even a garden.

They are "free play" (there are no goals, other than having fun).

Drag the dolls, make them spin and fly. Sit them in the chairs. Move the scenery about.

I like to drop a doll into the glass vase, and then shrink to vase,
so the doll is trapped, like a ship in a bottle.

When you first enter the doll's house, the room is fairly bare. Not many dolls or other objects.
There will be one picture pinned to the cork-board. Click it to play the first game.
If you win, you'll return to the doll's house, and as a reward, you will get one or two new objects or dolls to play with.
You will also get a new picture on the cork-board, leading to a new level.

When all levels are complete, the doll's house will be full.
Play around for a bit, or move on to another doll's house.

It's Gonna Rain
---------------

[![It's Gonna Rain](rainMini.png)](rain.png)

This is a bonus game. It's chucking it down - what foul weather.
Don't let Foxie get wet, or she'll come apart at the seams.
(though I do like to watch the pieces flying around on their own!)

Very simple, yet very more-ish!

On the first level you drag Annie by the middle,
but next, you drag Foxie by the hand, and
if you aren't careful her arms and legs will flail wildly.
Whatever you do, don't get into a spin!

Gentle mouse movements are the key, but as the game progresses,
the rain gets more intense, and the doll starts to grow, it's only a
matter of time before you hit a rain drop.

Get in the mood, and have Steve Reich's
[It's Gonna Rain](https://www.youtube.com/watch?v=vugqRAX7xQE)
playing in the background!

Or if you prefer something more traditional, why not try
[Singin' in the Rain](https://www.youtube.com/watch?v=swloMVFALXw)

